# Aide et ressources de JupyLabBook pour Synchrotron SOLEIL

[<img src="https://avatars3.githubusercontent.com/u/7388996?v=4&s=400" width="150"/>](https://github.com/ArnaudHemmerle/JupyLabBook)

## Résumé

- Notebook python, pour la réduction, lecture fichiers nexus
- Conversion en texte
- Visualisation
- Créé à Synchrotron Soleil

## Sources

- Code source:  https://github.com/ArnaudHemmerle/JupyLabBook 
- Documentation officielle:  https://github.com/ArnaudHemmerle/JupyLabBook/blob/master/docs/JupyLabBook_User_Manual.pdf

## Navigation rapide

| Fichiers téléchargeables | Tutoriaux |
| - | - |
| [Jeux de données](https://github.com/ArnaudHemmerle/JupyFluo/tree/master/example) | [Tutoriaux officiels](https://github.com/ArnaudHemmerle/JupyLabBook/blob/master/docs/JupyLabBook_User_Manual.pdf) |

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS
- Installation: Difficile (très technique),  support difficile

## Format de données

- en entrée: nexus 
- en sortie: texte,  images
- sur la Ruche,  sur la Ruche locale
